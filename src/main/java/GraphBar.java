/*
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;
import java.awt.*;

public class GraphBar extends JFrame {

    public GraphBar(String chartTitle, String y, int positive_syriza, int negative_syriza, int positive_nd, int negative_nd, int positive_tsipras, int negative_tsipras, int positive_mitsotakis, int negative_mitsotakis) {
        super("Tweets Statistics");
        JFreeChart barChart = ChartFactory.createBarChart(
                chartTitle,
                "",
                y,
                createDataset(positive_syriza, negative_syriza, positive_nd, negative_nd, positive_tsipras, negative_tsipras, positive_mitsotakis, negative_mitsotakis),
                PlotOrientation.VERTICAL,
                true, true, false);

        ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(560, 367));

        //customise chart
        CategoryPlot plot = (CategoryPlot) barChart.getPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.GREEN);

        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(0, Color.BLUE);
        renderer.setSeriesPaint(1, Color.GREEN);
        renderer.setSeriesPaint(2, Color.RED);
        renderer.setDrawBarOutline(false);
        renderer.setItemMargin(0); //Only 0 works.

        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setContentPane(chartPanel);
    }

    public GraphBar(String chartTitle, String y, double positive_syriza, double negative_syriza, double positive_nd, double negative_nd, double positive_tsipras, double negative_tsipras, double positive_mitsotakis, double negative_mitsotakis) {
        super("Tweets Statistics");
        JFreeChart barChart = ChartFactory.createBarChart(
                chartTitle,
                "",
                y,
                createDataset(positive_syriza, negative_syriza, positive_nd, negative_nd, positive_tsipras, negative_tsipras, positive_mitsotakis, negative_mitsotakis),
                PlotOrientation.VERTICAL,
                true, true, false);

        ChartPanel chartPanel = new ChartPanel(barChart);

        //customise chart
        CategoryPlot plot = (CategoryPlot) barChart.getPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.GREEN);

        BarRenderer renderer = (BarRenderer) plot.getRenderer();
        renderer.setSeriesPaint(0, Color.GREEN);
        renderer.setSeriesPaint(1, Color.RED);
        renderer.setDrawBarOutline(false);
        renderer.setItemMargin(0); //Only 0 works.

        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setContentPane(chartPanel);
    }

    private CategoryDataset createDataset(int positive_syriza, int negative_syriza, int positive_nd, int negative_nd, int positive_tsipras, int negative_tsipras, int positive_mitsotakis, int negative_mitsotakis) {
        final DefaultCategoryDataset dataset =
                new DefaultCategoryDataset();

        dataset.addValue(positive_syriza + negative_syriza, "Total", "#Syriza");
        dataset.addValue(positive_syriza, "Positive", "#Syriza");
        dataset.addValue(negative_syriza, "Negative", "#Syriza");

        dataset.addValue(positive_nd + negative_nd, "Total", "#ND");
        dataset.addValue(positive_nd, "Positive", "#ND");
        dataset.addValue(negative_nd, "Negative", "#ND");

        dataset.addValue(positive_tsipras + negative_tsipras, "Total", "@atsipras");
        dataset.addValue(positive_tsipras, "Positive", "@atsipras");
        dataset.addValue(negative_tsipras, "Negative", "@atsipras");

        dataset.addValue(positive_mitsotakis + negative_mitsotakis, "Total", "@kmitsotakis");
        dataset.addValue(positive_mitsotakis, "Positive", "@kmitsotakis");
        dataset.addValue(negative_mitsotakis, "Negative", "@kmitsotakis");

        return dataset;
    }

    private CategoryDataset createDataset(double positive_syriza, double negative_syriza, double positive_nd, double negative_nd, double positive_tsipras, double negative_tsipras, double positive_mitsotakis, double negative_mitsotakis) {
        final DefaultCategoryDataset dataset =
                new DefaultCategoryDataset();

        dataset.addValue(positive_syriza, "positive", "#syriza");
        dataset.addValue(negative_syriza, "negative", "#syriza");

        dataset.addValue(positive_nd, "positive", "#nd");
        dataset.addValue(negative_nd, "negative", "#nd");

        dataset.addValue(positive_tsipras, "positive", "@atsipras");
        dataset.addValue(negative_tsipras, "negative", "@atsipras");

        dataset.addValue(positive_mitsotakis, "positive", "@kmitsotakis");
        dataset.addValue(negative_mitsotakis, "negative", "@kmitsotakis");

        return dataset;
    }
}
*/
