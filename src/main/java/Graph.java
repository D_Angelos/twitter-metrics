/*
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.swing.*;

public class Graph extends JFrame {
    int run = 1;

    public Graph(String title, String x, String y, int[] syriza, int[] nd, int[] tsipras, int[] mitsotakis) {
        super("Tweets Statistics");
        JFreeChart lineChart = ChartFactory.createLineChart(
                title,
                x, y,
                createDataset(syriza, nd, tsipras, mitsotakis),
                PlotOrientation.VERTICAL,
                true, true, false);

        ChartPanel chartPanel = new ChartPanel(lineChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(540, 367));

        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setContentPane(chartPanel);
    }

    private DefaultCategoryDataset createDataset(int[] syriza, int[] nd, int[] tsipras, int[] mitsotakis) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        dataset.addValue(syriza[0], "#Syriza", "01-19");
        dataset.addValue(syriza[1], "#Syriza", "01-20");
        dataset.addValue(syriza[2], "#Syriza", "01-21");
        dataset.addValue(syriza[3], "#Syriza", "01-22");
        dataset.addValue(syriza[4], "#Syriza", "01-23");
        dataset.addValue(syriza[5], "#Syriza", "01-24");
        dataset.addValue(syriza[6], "#Syriza", "01-25");

        dataset.addValue(nd[0], "#ND", "01-19");
        dataset.addValue(nd[1], "#ND", "01-20");
        dataset.addValue(nd[2], "#ND", "01-21");
        dataset.addValue(nd[3], "#ND", "01-22");
        dataset.addValue(nd[4], "#ND", "01-23");
        dataset.addValue(nd[5], "#ND", "01-24");
        dataset.addValue(nd[6], "#ND", "01-25");

        dataset.addValue(tsipras[0], "@atsiptas", "01-19");
        dataset.addValue(tsipras[1], "@atsiptas", "01-20");
        dataset.addValue(tsipras[2], "@atsiptas", "01-21");
        dataset.addValue(tsipras[3], "@atsiptas", "01-22");
        dataset.addValue(tsipras[4], "@atsiptas", "01-23");
        dataset.addValue(tsipras[5], "@atsiptas", "01-24");
        dataset.addValue(tsipras[6], "@atsiptas", "01-25");

        dataset.addValue(mitsotakis[0], "@kmitsotakis", "01-19");
        dataset.addValue(mitsotakis[1], "@kmitsotakis", "01-20");
        dataset.addValue(mitsotakis[2], "@kmitsotakis", "01-21");
        dataset.addValue(mitsotakis[3], "@kmitsotakis", "01-22");
        dataset.addValue(mitsotakis[4], "@kmitsotakis", "01-23");
        dataset.addValue(mitsotakis[5], "@kmitsotakis", "01-24");
        dataset.addValue(mitsotakis[6], "@kmitsotakis", "01-25");

        return dataset;
    }
}*/
