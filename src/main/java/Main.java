import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import twitter4j.*;
import twitter4j.conf.ConfigurationBuilder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.text.Normalizer;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static twitter4j.Query.RECENT;

public class Main {

    //authentication info
    private static String ConsumerKey = ""; // insert consumer key
    private static String ConsumerSecret = ""; // insert consumer secret
    private static String AccessToken = "-"; // insert access token
    private static String AccessTokenSecret = ""; // insert access token secret

    //search queries
    private static String all = "#SYRIZA OR #NewDemocracy OR #Syrizanel OR @realDonaldTrump OR @HillaryClinton OR #ND -RT";
    private static String tsipras = "@atsipras";
    private static String syriza = "#SYRIZA";
    private static String nd = "#ND";
    private static String newDemocracy = "#NewDemocracy";
    private static String syrizanel = "#Syrizanel";
    private static String mitsotakis = "@kmitsotakis";

    //private static int maxQueryNumber = 100;
    private static int tweetsPerQuery = 100;

    //store tweets
    private ArrayList<String> tweets = new ArrayList<String>();

    //store tweets to check for duplicates
    private static ArrayList<String> tweets_data = new ArrayList<>();

    //store positive and negative words with their polarity
    private ArrayList<String> pos_arraylist = new ArrayList<>();
    private ArrayList<String> neg_arraylist = new ArrayList<>();
    private static HashMap<String, Integer> pos_hashmap_syriza = new HashMap<>();
    private static HashMap<String, Integer> pos_hashmap_nd = new HashMap<>();
    private static HashMap<String, Integer> pos_hashmap_tsipras = new HashMap<>();
    private static HashMap<String, Integer> pos_hashmap_mitsotakis = new HashMap<>();
    private static HashMap<String, Integer> neg_hashmap_syriza = new HashMap<>();
    private static HashMap<String, Integer> neg_hashmap_nd = new HashMap<>();
    private static HashMap<String, Integer> neg_hashmap_tsipras = new HashMap<>();
    private static HashMap<String, Integer> neg_hashmap_mitsotakis = new HashMap<>();

    //variables for xml
    private static ArrayList<String[]> tweets_xml = new ArrayList<>();
    private static ArrayList<String[]> tweets_read_xml = new ArrayList<>();

    //arraylist to store stopwords
    private static ArrayList<String> stopwords = new ArrayList<>();

    //variables to calculate average
    private static int total_number_of_positive_tweets = 0;
    private static int total_number_of_negative_tweets = 0;
    private static boolean runned = false;
    private static double average = 0;

    //variables for s
    private static int times_runned = 0;
    private double[] standard_deviation = new double[8];

    //a lot of variables for s calculation :P for each day
    //array with {positive_syriza, negative_syriza, positive_nd, negative_nd, positive_tsipras, negative_tsipras, positive_mitsotakis, negative_mitsotakis} for each day
    private int[] day1 = new int[8], day2 = new int[8], day3 = new int[8], day4 = new int[8], day5 = new int[8], day6 = new int[8], day7 = new int[8];


    public static void main(String[] args) throws TwitterException, InterruptedException, IOException, ParseException {

        long ID = -1;
        int total = 0;
        int tweet_counter = 100;
        int counter = 0;

        //create instance of class to call the non-static methods
        Main sT = new Main();
/*
        int n = 0;
        do {
            Scanner reader = new Scanner(System.in);  // Reading from System.in
            System.out.println("What date do you want to see?(Choose 1-8)\n1.2017-01-19\n2.2017-01-20\n3.2017-01-21\n4.2017-01-22\n5.2017-01-23\n6.2017-01-24\n7.2017-01-25\n8.From 2017-01-19 till 2017-01-25");
            System.out.print("Option: ");
            n = reader.nextInt(); // Scans the next token of the input as an int.
            if (n < 1 || n > 8) System.out.println("No such option.");
        } while (n < 1 || n > 8);

        switch (n) {
            case 1:
                System.out.println("************************************\nStatistics for 2017-01-19\n");
                break;
            case 2:
                System.out.println("************************************\nStatistics for 2017-01-20\n");
                break;
            case 3:
                System.out.println("************************************\nStatistics for 2017-01-21\n");
                break;
            case 4:
                System.out.println("************************************\nStatistics for 2017-01-22\n");
                break;
            case 5:
                System.out.println("************************************\nStatistics for 2017-01-23\n");
                break;
            case 6:
                System.out.println("************************************\nStatistics for 2017-01-24\n");
                break;
            case 7:
                System.out.println("************************************\nStatistics for 2017-01-25\n");
                break;
            case 8:
                System.out.println("************************************\nStatistics for all week(2017-01-19 - 2017-01-25)\n");
                break;
            default:
                break;
        }
*/
        sT.readStopwords();

        //authenticate and configure
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthConsumerKey(ConsumerKey)
                .setOAuthConsumerSecret(ConsumerSecret)
                .setOAuthAccessToken(AccessToken)
                .setOAuthAccessTokenSecret(AccessTokenSecret);
        TwitterFactory tf = new TwitterFactory(cb.build());
        Twitter twitter = tf.getInstance();

        //set Date one week before now
//        Calendar cal = Calendar.getInstance();                                            // ***************************************
//        cal.add(Calendar.DATE, -1);                                                       // *Comment if date limitation not needed*
//        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());    // ***************************************

        //create query
        Query query = new Query(all);
        //fetch tweets one week ago
//        query.setSince(modifiedDate);                                                     // Comment if date limitation not needed
        //query.setLang("el");
        query.setSince("2017-05-09");
        query.setUntil("2017-05-13");

        //take the limit of tweets per query
        Map<String, RateLimitStatus> rateLimitStatus = twitter.getRateLimitStatus("search");
        RateLimitStatus limit = rateLimitStatus.get("/search/tweets");
        System.out.println("Fetching tweets...");

        do{
            //if we reach limit
            //if (limit.getRemaining() == 0) {
            //    System.out.println("!!! Sleeping for "+(limit.getSecondsUntilReset()+60)+" seconds due to rate limits !!!\n");
                //sleep until we can fetch more tweets
                Thread.sleep(10000);
            //}

            //max tweets to fetch per query
            query.setCount(tweetsPerQuery);
            //fetch more recent first
            query.resultType(RECENT);

            //if ID = -1 then this is the first run, else make last id the max ID
            if (ID != -1) {
                query.setMaxId(ID - 1);
            }

            //Search query
            QueryResult result = twitter.search(query);

            //check size of tweets is being fetched in order to stop
            tweet_counter = result.getTweets().size();
            System.out.println("Fetched block with "+tweet_counter+" tweets.");

            for (Status tweet : result.getTweets()) {
                //	Increment our count of tweets retrieved
                total++;

                //change the maxID to fetch different blocks of tweets
                if (ID == -1 || tweet.getId() < ID) {
                    ID = tweet.getId();
                }

                //check for duplicates
                if(!tweets_data.contains(stopwords(apokopi(stripAccents(removeUrl(tweet.getText()).replaceAll("[(){},.;!?<>%#@]", " ").replaceAll("\\d","").replaceAll("\\s+", " ").toUpperCase()))))) {
                    //store data needed in different arrays
                    tweets_data.add(stopwords(apokopi(stripAccents(removeUrl(tweet.getText()).replaceAll("[(){},.;!?<>%#@]", " ").replaceAll("\\d", "").replaceAll("\\s+", " ").toUpperCase()))));
                    //store mentions to string to save them seperately
                    String hashtags_xml = "";
                    for(HashtagEntity hashtagEntity : tweet.getHashtagEntities()){
                        hashtags_xml += hashtagEntity.getText()+" ";
                    }
                    //store mentions to string to save them seperately
                    String mentions_xml = "";
                    for(UserMentionEntity userMentionEntity : tweet.getUserMentionEntities()){
                        mentions_xml += userMentionEntity.getText()+" ";
                    }
                    if (counter > 0)
                    counter = 0;

                    //arraylist with tweets to store
                    tweets_xml.add(new String[]{stopwords(apokopi(stripAccents(removeUrl(tweet.getText()).replaceAll("[(){},.;!?<>%#@]", " ").replaceAll("\\d", "").replaceAll("\\s+", " ").toUpperCase()))), tweet.getUser().getName(), tweet.getCreatedAt().toString(), Long.toString(tweet.getId()), hashtags_xml, mentions_xml});
                }
            }

            //check if the thread needs to sleep or not
            limit = result.getRateLimitStatus();

        } while(tweet_counter - 99 > 0);

        System.out.println();

        //read the tweets stored in file
        sT.saveToXML(tweets_xml,"8");
/*
        //create table with words from excels
        sT.readXls();

        sT.readXML(n);

        sT.calculateMood();

        //calculate positive or negative for each one
        sT.calculateFrequency();

        //runs only first time to create the txts
*/      sT.writeFrequencies("syriza_pos_frequencies.txt", pos_hashmap_syriza);
        sT.writeFrequencies("syriza_neg_frequencies.txt", neg_hashmap_syriza);
        sT.writeFrequencies("nd_pos_frequencies.txt", pos_hashmap_nd);
        sT.writeFrequencies("nd_neg_frequencies.txt", neg_hashmap_nd);
        sT.writeFrequencies("tsipras_pos_frequencies.txt", pos_hashmap_tsipras);
        sT.writeFrequencies("tsipras_neg_frequencies.txt", neg_hashmap_tsipras);
        sT.writeFrequencies("mitsotakis_pos_frequencies.txt", pos_hashmap_mitsotakis);
        sT.writeFrequencies("mitsotakis_neg_frequencies.txt", neg_hashmap_mitsotakis);
/*
        sT.findS();
        sT.chooseGraph();
 */   }

    /**
     * method that removes the url from the string
     *
     * @param s input string to edit
     * @return string without the url
     */
    private static String removeUrl(String s) {
        String urlPattern = "((https?|ftp|gopher|telnet|file|Unsure|http):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern p = Pattern.compile(urlPattern, Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(s);
        int i = 0;
        while (m.find()) {
            s = s.replaceAll(m.group(i), "").trim();
            i++;
        }
        return s;
    }

    /**
     * remove accents from string
     * used for the accents of the upper case letters
     *
     * @param s input string to edit
     * @return string without the accents
     */
    public static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }

    /**
     * uses GreekStremmer to cut words
     *
     * @param s input string to edit
     * @return the new string that its end has changed
     */
    private static String apokopi(String s) {

        GreekStemmer gs = new GreekStemmer();

        String[] words = s.split("\\s+");

        for (int i = 0; i < words.length; i++) {
            words[i] = gs.stem(words[i]);
        }
        //make a string from a string array (Java 8 method)
        s = Arrays.stream(words).collect(Collectors.joining(" "));

        return s;
    }

    /**
     * read the words from the excels and stores them into arraylists
     *
     * @throws InterruptedException
     */
    private void readXls() throws InterruptedException {

        HSSFWorkbook xlWBook;
        HSSFSheet xlSheet;
        HSSFRow xlRow;
        HSSFCell xlCell;
        String fileName = "PosLex.xls";
        int noOfRows = 0;
        int noOfColumns = 0;
        String[][] excelData = new String[1][1];

        for (int i = 1; i <= 2; i++) {
            if (i == 1) fileName = "PosLex.xls";
            else fileName = "NegLex.xls";
            try {

                FileInputStream xlFile = new FileInputStream(fileName);

                // Access the required test data sheet

                xlWBook = new HSSFWorkbook(xlFile);

                // Assuming your data is in Sheet1- if not use your own sheet name
                xlSheet = xlWBook.getSheet("Tablib Dataset");

                // gives row count in sheet
                noOfRows = xlSheet.getPhysicalNumberOfRows();

                // gives column count in sheet
                xlRow = xlSheet.getRow(0);
                noOfColumns = xlRow.getLastCellNum();

                // excelData - 2 dimm array - stores all the excel data -Sheet1 only
                excelData = new String[noOfRows][noOfColumns];

                // r - row c- column
                for (int r = 1; r < noOfRows; r++) {
                    //for (int c = 0; c < noOfColumns; c++) {
                    xlRow = xlSheet.getRow(r);
                    //xlCell = xlRow.getCell(c);

                    // Here we have complete excel data in an array -excelData-

                    //excelData[r][c] = xlCell.getStringCellValue();
                    excelData[r][0] = xlRow.getCell(0).getStringCellValue();
                    excelData[r][1] = Double.toString(xlRow.getCell(1).getNumericCellValue());
                    excelData[r][2] = xlRow.getCell(2).getStringCellValue();

                    if (i == 1) {
                        pos_arraylist.add(excelData[r][2]);
                        pos_hashmap_syriza.put(excelData[r][2], 1);
                        pos_hashmap_nd.put(excelData[r][2], 1);
                        pos_hashmap_tsipras.put(excelData[r][2], 1);
                        pos_hashmap_mitsotakis.put(excelData[r][2], 1);
                    } else {
                        neg_arraylist.add(excelData[r][2]);
                        neg_hashmap_syriza.put(excelData[r][2], 1);
                        neg_hashmap_nd.put(excelData[r][2], 1);
                        neg_hashmap_tsipras.put(excelData[r][2], 1);
                        neg_hashmap_mitsotakis.put(excelData[r][2], 1);
                    }
                    //System.out.println("row: " + r);
                    //System.out.println(excelData[r][0] + " " + excelData[r][1] + " " + excelData[r][2]);
                    //}
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * calculate how many times each word is shown in every query
     */
    private void calculateFrequency() {
        for (String[] s1 : tweets_read_xml) { //take every tweet
            String[] words1 = s1[0].split("\\s+"); //split the words
            for (int i = 0; i < words1.length; i++) { //for every word
                if (pos_arraylist.contains(words1[i])) { //if there is a positive word
                    String[] words2 = s1[4].split("\\s+"); //split the hashtags of tweet
                    for (int ii = 0; ii < words2.length; ii++) { //for every hashtag
                        //increment the word frequency for the right hashtag
                        if (words2[ii].toLowerCase().equals("syriza"))
                            pos_hashmap_syriza.put(words1[i], pos_hashmap_syriza.get(words1[i]) + 1);
                        else if (words2[ii].toLowerCase().equals("nd"))
                            pos_hashmap_nd.put(words1[i], pos_hashmap_nd.get(words1[i]) + 1);
                        else if (words2[ii].toLowerCase().equals("syrizanel"))
                            pos_hashmap_syriza.put(words1[i], pos_hashmap_syriza.get(words1[i]) + 1);
                        else if (words2[ii].toLowerCase().contains("newdemocracy"))
                            pos_hashmap_nd.put(words1[i], pos_hashmap_nd.get(words1[i]) + 1);
                    }
                    String[] words3 = s1[5].split("\\s+"); //split the mentions
                    for (int ii = 0; ii < words3.length; ii++) { //for every mention
                        //increment the word frequency for the right mention
                        if (words3[ii].toLowerCase().equals("atsipras"))
                            pos_hashmap_tsipras.put(words1[i], pos_hashmap_tsipras.get(words1[i]) + 1);
                        if (words3[ii].toLowerCase().equals("kmitsotakis"))
                            pos_hashmap_mitsotakis.put(words1[i], pos_hashmap_mitsotakis.get(words1[i]) + 1);
                    }
                }
                //same as the above but for negative words
                if (neg_arraylist.contains(words1[i])) {
                    String[] words2 = s1[4].split("\\s+");
                    for (int ii = 0; ii < words2.length; ii++) {
                        if (words2[ii].toLowerCase().equals("syriza"))
                            neg_hashmap_syriza.put(words1[i], neg_hashmap_syriza.get(words1[i]) + 1);
                        else if (words2[ii].toLowerCase().equals("nd"))
                            neg_hashmap_nd.put(words1[i], neg_hashmap_nd.get(words1[i]) + 1);
                        else if (words2[ii].toLowerCase().equals("syrizanel"))
                            neg_hashmap_syriza.put(words1[i], neg_hashmap_syriza.get(words1[i]) + 1);
                        else if (words2[ii].toLowerCase().contains("newdemocracy"))
                            neg_hashmap_nd.put(words1[i], neg_hashmap_nd.get(words1[i]) + 1);
                    }
                    String[] words3 = s1[5].split("\\s+");
                    for (int ii = 0; ii < words3.length; ii++) {
                        if (words3[ii].toLowerCase().equals("atsipras"))
                            neg_hashmap_tsipras.put(words1[i], neg_hashmap_tsipras.get(words1[i]) + 1);
                        if (words3[ii].toLowerCase().equals("kmitsotakis"))
                            neg_hashmap_mitsotakis.put(words1[i], neg_hashmap_mitsotakis.get(words1[i]) + 1);
                    }
                }
            }
        }
    }

    /**
     * calculates the positives and negatives tweets for each category
     */
    private void calculateMood() {

        //positive and negative counters
        int syriza_positive_counter = 0, syriza_negative_counter = 0, atsipras_positive_counter = 0, atsipras_negative_counter = 0, nd_positive_counter = 0, nd_negative_counter = 0, mitsotakis_positive_counter = 0, mitsotakis_negative_counter = 0;

        //varibale to check if tweet is positive or negative
        int count = 0;

        for (String[] s1 : tweets_read_xml) { //for every tweet
            String[] words1 = s1[0].split("\\s+"); //split the words
            for (int i = 0; i < words1.length; i++) { //for every word
                if (pos_arraylist.contains(words1[i])) { //if there is a positive word increment counter
                    count++;
                }
                if (neg_arraylist.contains(words1[i])) { //if there is a negative word decrease counter
                    count--;
                }
            }

            if (count > 0) { //if tweet is positive
                String[] words2 = s1[4].split("\\s+"); //take hashtags
                for (int ii = 0; ii < words2.length; ii++) { //increment counter for the right hashtag
                    if (words2[ii].toLowerCase().equals("syriza") || words2[ii].toLowerCase().equals("syrizanel"))
                        syriza_positive_counter++;
                    if (words2[ii].toLowerCase().equals("nd") || words2[ii].toLowerCase().contains("newdemocracy"))
                        nd_positive_counter++;
                }
                String[] words3 = s1[5].split("\\s+"); //take mentions
                for (int ii = 0; ii < words3.length; ii++) { //increment counter for the right mention
                    if (words3[ii].toLowerCase().equals("atsipras"))
                        atsipras_positive_counter++;
                    if (words3[ii].toLowerCase().equals("kmitsotakis"))
                        mitsotakis_positive_counter++;
                }
            } else { //if tweet is negative or neutral do same as above
                String[] words2 = s1[4].split("\\s+");
                for (int ii = 0; ii < words2.length; ii++) {
                    if (words2[ii].toLowerCase().equals("syriza") || words2[ii].toLowerCase().equals("syrizanel"))
                        syriza_negative_counter++;
                    if (words2[ii].toLowerCase().equals("nd") || words2[ii].toLowerCase().contains("newdemocracy"))
                        nd_negative_counter++;
                }
                String[] words3 = s1[5].split("\\s+");
                for (int ii = 0; ii < words3.length; ii++) {
                    if (words3[ii].toLowerCase().equals("atsipras"))
                        atsipras_negative_counter++;
                    if (words3[ii].toLowerCase().equals("kmitsotakis"))
                        mitsotakis_negative_counter++;
                }
            }
            count = 0;
        }

        if (!runned) { //in order not to print twice when average runs
            System.out.println("Positive syriza: " + syriza_positive_counter +
                    "\nNegative syriza: " + syriza_negative_counter +
                    "\nPositive ND: " + nd_positive_counter +
                    "\nNegative ND: " + nd_negative_counter +
                    "\nPositive Tsipras: " + atsipras_positive_counter +
                    "\nNegative Tsipras: " + atsipras_negative_counter +
                    "\nPositive Mitsotakis: " + mitsotakis_positive_counter +
                    "\nNegative Mitsotakis: " + mitsotakis_negative_counter);
            runned = true;
        }

        if (times_runned == 1) { //run from the 3rd run and store each days counters into arrays in order to use them in standard variation
            day1[0] = syriza_positive_counter;
            day1[1] = syriza_negative_counter;
            day1[2] = nd_positive_counter;
            day1[3] = nd_negative_counter;
            day1[4] = atsipras_positive_counter;
            day1[5] = atsipras_negative_counter;
            day1[6] = mitsotakis_positive_counter;
            day1[7] = mitsotakis_negative_counter;
        } else if (times_runned == 2) {
            day2[0] = syriza_positive_counter;
            day2[1] = syriza_negative_counter;
            day2[2] = nd_positive_counter;
            day2[3] = nd_negative_counter;
            day2[4] = atsipras_positive_counter;
            day2[5] = atsipras_negative_counter;
            day2[6] = mitsotakis_positive_counter;
            day2[7] = mitsotakis_negative_counter;
        } else if (times_runned == 3) {
            day3[0] = syriza_positive_counter;
            day3[1] = syriza_negative_counter;
            day3[2] = nd_positive_counter;
            day3[3] = nd_negative_counter;
            day3[4] = atsipras_positive_counter;
            day3[5] = atsipras_negative_counter;
            day3[6] = mitsotakis_positive_counter;
            day3[7] = mitsotakis_negative_counter;
        } else if (times_runned == 4) {
            day4[0] = syriza_positive_counter;
            day4[1] = syriza_negative_counter;
            day4[2] = nd_positive_counter;
            day4[3] = nd_negative_counter;
            day4[4] = atsipras_positive_counter;
            day4[5] = atsipras_negative_counter;
            day4[6] = mitsotakis_positive_counter;
            day4[7] = mitsotakis_negative_counter;
        } else if (times_runned == 5) {
            day5[0] = syriza_positive_counter;
            day5[1] = syriza_negative_counter;
            day5[2] = nd_positive_counter;
            day5[3] = nd_negative_counter;
            day5[4] = atsipras_positive_counter;
            day5[5] = atsipras_negative_counter;
            day5[6] = mitsotakis_positive_counter;
            day5[7] = mitsotakis_negative_counter;
        } else if (times_runned == 6) {
            day6[0] = syriza_positive_counter;
            day6[1] = syriza_negative_counter;
            day6[2] = nd_positive_counter;
            day6[3] = nd_negative_counter;
            day6[4] = atsipras_positive_counter;
            day6[5] = atsipras_negative_counter;
            day6[6] = mitsotakis_positive_counter;
            day6[7] = mitsotakis_negative_counter;
        } else if (times_runned == 7) {
            day7[0] = syriza_positive_counter;
            day7[1] = syriza_negative_counter;
            day7[2] = nd_positive_counter;
            day7[3] = nd_negative_counter;
            day7[4] = atsipras_positive_counter;
            day7[5] = atsipras_negative_counter;
            day7[6] = mitsotakis_positive_counter;
            day7[7] = mitsotakis_negative_counter;
        }
        times_runned++;
    }

    /**
     * read stopwords from file and store them into an arraylist
     */
    private void readStopwords() {
        Scanner s = null;
        try {
            s = new Scanner(new File("greekstopwords.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (s.hasNext()) {
            stopwords.add(s.next());
        }
        s.close();
    }

    /**
     * check if a string contains stopwords
     *
     * @param s input string to edit
     * @return string without the stopwords
     */
    private static String stopwords(String s) {
        String[] words = s.split("\\s+");

        for (int i = 0; i < words.length; i++) {
            if (stopwords.contains(words[i])) {
                words[i] = "";
            }
        }

        //method to make a string out of an array, want java 8 to work
        s = Arrays.stream(words).collect(Collectors.joining(" "));

        return s;
    }

    /**
     * write an arraylist to xml like:
     * <data>
     * <tweets>
     * <tweet></tweet>
     * <name></name>
     * <date></date>
     * <id></id>
     * <hashtags></hashtags>
     * <mentions></mentions>
     * </tweets>
     * </data>
     *
     * @param list with tweets that will be stored
     */
    private void saveToXML(ArrayList<String[]> list, String num) {

        Document doc;
        Element root;

        try {

            File f = new File("./tweets/tweets" + num + ".xml");
            if (!f.exists()) {
                DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
                DocumentBuilder build = dFact.newDocumentBuilder();
                doc = build.newDocument();
                root = doc.createElement("data");
                doc.appendChild(root);
            } else {
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
                doc = documentBuilder.parse("./tweets/tweets" + num + ".xml");
                root = doc.getDocumentElement();
            }

            for (String[] s : list) {

                Element tweets = doc.createElement("tweets");
                root.appendChild(tweets);

                Element tweet = doc.createElement("tweet");
                tweet.appendChild(doc.createTextNode(s[0]));
                tweets.appendChild(tweet);

                Element name = doc.createElement("name");
                name.appendChild(doc.createTextNode(s[1]));
                tweets.appendChild(name);

                Element date = doc.createElement("date");
                date.appendChild(doc.createTextNode(s[2]));
                tweets.appendChild(date);

                Element id = doc.createElement("id");
                id.appendChild(doc.createTextNode(s[3]));
                tweets.appendChild(id);

                Element hashtags = doc.createElement("hashtags");
                hashtags.appendChild(doc.createTextNode(s[4]));
                tweets.appendChild(hashtags);

                Element mentions = doc.createElement("mentions");
                mentions.appendChild(doc.createTextNode(s[5]));
                tweets.appendChild(mentions);
            }

            // Save the document to the disk file
            TransformerFactory tranFactory = TransformerFactory.newInstance();
            Transformer aTransformer = tranFactory.newTransformer();

            DOMSource source = new DOMSource(doc);


            // format the XML nicely
            aTransformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            aTransformer.setOutputProperty(
                    "{http://xml.apache.org/xslt}indent-amount", "4");
            aTransformer.setOutputProperty(OutputKeys.INDENT, "yes");

            // location and name of XML file you can change as per need
            FileWriter fos = new FileWriter("./tweets/tweets" + num + ".xml");
            StreamResult result = new StreamResult(fos);
            aTransformer.transform(source, result);
        } catch (TransformerException ex) {
            System.out.println("Error outputting document");

        } catch (ParserConfigurationException ex) {
            System.out.println("Error building document");
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * read from the xml and stores it into an array
     *
     * @param num what xml to read depending the day
     */
    private void readXML(int num) {
        String[] read_from_xml = new String[6];
        tweets_read_xml.clear(); //used for calculate average for the 2nd run
        try {

            File fXmlFile = new File("./tweets/tweets" + num + ".xml");
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("tweets");

            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                    Element eElement = (Element) nNode;
                    read_from_xml = new String[6];
                    read_from_xml[0] = eElement.getElementsByTagName("tweet").item(0).getTextContent();
                    read_from_xml[1] = eElement.getElementsByTagName("name").item(0).getTextContent();
                    read_from_xml[2] = eElement.getElementsByTagName("date").item(0).getTextContent();
                    read_from_xml[3] = eElement.getElementsByTagName("id").item(0).getTextContent();
                    read_from_xml[4] = eElement.getElementsByTagName("hashtags").item(0).getTextContent();
                    read_from_xml[5] = eElement.getElementsByTagName("mentions").item(0).getTextContent();
                }
                tweets_read_xml.add(read_from_xml);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * create txts with the positive or negative words with the number shown in tweets
     * it contains every day and each one is splitted with ********
     * first one is the first day, second one the second day etc..
     * the last one is all the days together
     *
     * @param file name of file
     * @param hm   the hashmap with the words and initial values
     * @throws IOException
     */
    private void writeFrequencies(String file, HashMap<String, Integer> hm) throws IOException {

        FileWriter fstream;
        BufferedWriter out;

        // create your filewriter and bufferedreader
        fstream = new FileWriter("frequencies/" + file, true);
        out = new BufferedWriter(fstream);

        // initialize the record count
        int count = 0;

        // create your iterator for your map
        Iterator<Map.Entry<String, Integer>> it = hm.entrySet().iterator();

        // then use the iterator to loop through the map, stopping when we reach the
        // last record in the map or when we have printed enough records
        while (it.hasNext()) {

            // the key/value pair is stored here in pairs
            Map.Entry<String, Integer> pairs = it.next();

            // since you only want the value, we only care about pairs.getValue(), which is written to out
            out.write(pairs.getKey() + ": " + pairs.getValue() + "\n");
        }
        out.write("**********\n");
        // lastly, close the file and end
        out.close();
    }

    /**
     * calculate standard deviation for all categories
     * calculates also the average positive and negative tweets per day
     */
    private void findS() {
        //standard deviation
        double s = 0;
        //varibles to calculate each average
        double average_positive_syriza = 0, average_negative_syriza = 0, average_positive_nd = 0, average_negative_nd = 0, average_positive_tsipras = 0, average_negative_tsipras = 0, average_positive_mitsotakis = 0, average_negative_mitsotakis = 0;

        //read all the xmls to create the arrays with negative and positive counters
        readXML(1);
        calculateMood();
        readXML(2);
        calculateMood();
        readXML(3);
        calculateMood();
        readXML(4);
        calculateMood();
        readXML(5);
        calculateMood();
        readXML(6);
        calculateMood();
        readXML(7);
        calculateMood();

        //calculate averages for each category
        average_positive_syriza = (day1[0] + day2[0] + day3[0] + day4[0] + day5[0] + day6[0] + day7[0]) / 7.0;
        average_negative_syriza = (day1[1] + day2[1] + day3[1] + day4[1] + day5[1] + day6[1] + day7[1]) / 7.0;
        average_positive_nd = (day1[2] + day2[2] + day3[2] + day4[2] + day5[2] + day6[2] + day7[2]) / 7.0;
        average_negative_nd = (day1[3] + day2[3] + day3[3] + day4[3] + day5[3] + day6[3] + day7[3]) / 7.0;
        average_positive_tsipras = (day1[4] + day2[4] + day3[4] + day4[4] + day5[4] + day6[4] + day7[4]) / 7.0;
        average_negative_tsipras = (day1[5] + day2[5] + day3[5] + day4[5] + day5[5] + day6[5] + day7[5]) / 7.0;
        average_positive_mitsotakis = (day1[6] + day2[6] + day3[6] + day4[6] + day5[6] + day6[6] + day7[6]) / 7.0;
        average_negative_mitsotakis = (day1[7] + day2[7] + day3[7] + day4[7] + day5[7] + day6[7] + day7[7]) / 7.0;

        System.out.println("************************************\nTotal:\n");
        System.out.println("Syriza: " + (day1[0] + day2[0] + day3[0] + day4[0] + day5[0] + day6[0] + day7[0] + day1[1] + day2[1] + day3[1] + day4[1] + day5[1] + day6[1] + day7[1]));
        System.out.println("ND: " + (day1[2] + day2[2] + day3[2] + day4[2] + day5[2] + day6[2] + day7[2] + day1[3] + day2[3] + day3[3] + day4[3] + day5[3] + day6[3] + day7[3]));
        System.out.println("Tsipras: " + (day1[4] + day2[4] + day3[4] + day4[4] + day5[4] + day6[4] + day7[4] + day1[5] + day2[5] + day3[5] + day4[5] + day5[5] + day6[5] + day7[5]));
        System.out.println("Mitsotakis: " + (day1[6] + day2[6] + day3[6] + day4[6] + day5[6] + day6[6] + day7[6] + day1[7] + day2[7] + day3[7] + day4[7] + day5[7] + day6[7] + day7[7]));

        System.out.println("************************************\nAverage:\n");
        System.out.println("Syriza positive: " + average_positive_syriza);
        System.out.println("Syriza negative: " + average_negative_syriza);
        System.out.println("ND positive: " + average_positive_nd);
        System.out.println("ND negative: " + average_negative_nd);
        System.out.println("Tsipras positive: " + average_positive_tsipras);
        System.out.println("Tsipras negative: " + average_negative_tsipras);
        System.out.println("Mitsotakis positive: " + average_positive_mitsotakis);
        System.out.println("Mitsotakis negative: " + average_negative_mitsotakis);

        System.out.println("************************************\nStandard deviation:\n");

        s = Math.sqrt(((day1[0] - average_positive_syriza) * (day1[0] - average_positive_syriza) +
                (day2[0] - average_positive_syriza) * (day2[0] - average_positive_syriza) +
                (day3[0] - average_positive_syriza) * (day3[0] - average_positive_syriza) +
                (day4[0] - average_positive_syriza) * (day4[0] - average_positive_syriza) +
                (day5[0] - average_positive_syriza) * (day5[0] - average_positive_syriza) +
                (day6[0] - average_positive_syriza) * (day6[0] - average_positive_syriza)) / 7.0);

        System.out.println("Syriza positive: " + s);
        standard_deviation[0] = s;

        s = Math.sqrt(((day1[1] - average_negative_syriza) * (day1[1] - average_negative_syriza) +
                (day2[1] - average_negative_syriza) * (day2[1] - average_negative_syriza) +
                (day3[1] - average_negative_syriza) * (day3[1] - average_negative_syriza) +
                (day4[1] - average_negative_syriza) * (day4[1] - average_negative_syriza) +
                (day5[1] - average_negative_syriza) * (day5[1] - average_negative_syriza) +
                (day6[1] - average_negative_syriza) * (day6[1] - average_negative_syriza)) / 7.0);

        System.out.println("Syriza negative: " + s);
        standard_deviation[1] = s;

        s = Math.sqrt(((day1[2] - average_positive_nd) * (day1[2] - average_positive_nd) +
                (day2[2] - average_positive_nd) * (day2[2] - average_positive_nd) +
                (day3[2] - average_positive_nd) * (day3[2] - average_positive_nd) +
                (day4[2] - average_positive_nd) * (day4[2] - average_positive_nd) +
                (day5[2] - average_positive_nd) * (day5[2] - average_positive_nd) +
                (day6[2] - average_positive_nd) * (day6[2] - average_positive_nd)) / 7.0);

        System.out.println("ND positive: " + s);
        standard_deviation[2] = s;

        s = Math.sqrt(((day1[3] - average_negative_nd) * (day1[3] - average_negative_nd) +
                (day2[3] - average_negative_nd) * (day2[3] - average_negative_nd) +
                (day3[3] - average_negative_nd) * (day3[3] - average_negative_nd) +
                (day4[3] - average_negative_nd) * (day4[3] - average_negative_nd) +
                (day5[3] - average_negative_nd) * (day5[3] - average_negative_nd) +
                (day6[3] - average_negative_nd) * (day6[3] - average_negative_nd)) / 7.0);

        System.out.println("ND negative: " + s);
        standard_deviation[3] = s;

        s = Math.sqrt(((day1[4] - average_positive_tsipras) * (day1[4] - average_positive_tsipras) +
                (day2[4] - average_positive_tsipras) * (day2[4] - average_positive_tsipras) +
                (day3[4] - average_positive_tsipras) * (day3[4] - average_positive_tsipras) +
                (day4[4] - average_positive_tsipras) * (day4[4] - average_positive_tsipras) +
                (day5[4] - average_positive_tsipras) * (day5[4] - average_positive_tsipras) +
                (day6[4] - average_positive_tsipras) * (day6[4] - average_positive_tsipras)) / 7.0);

        System.out.println("Tsipras positive: " + s);
        standard_deviation[4] = s;

        s = Math.sqrt(((day1[5] - average_negative_tsipras) * (day1[5] - average_negative_tsipras) +
                (day2[5] - average_negative_tsipras) * (day2[5] - average_negative_tsipras) +
                (day3[5] - average_negative_tsipras) * (day3[5] - average_negative_tsipras) +
                (day4[5] - average_negative_tsipras) * (day4[5] - average_negative_tsipras) +
                (day5[5] - average_negative_tsipras) * (day5[5] - average_negative_tsipras) +
                (day6[5] - average_negative_tsipras) * (day6[5] - average_negative_tsipras)) / 7.0);

        System.out.println("Tsipras negative: " + s);
        standard_deviation[5] = s;

        s = Math.sqrt(((day1[6] - average_positive_mitsotakis) * (day1[6] - average_positive_mitsotakis) +
                (day2[6] - average_positive_mitsotakis) * (day2[6] - average_positive_mitsotakis) +
                (day3[6] - average_positive_mitsotakis) * (day3[6] - average_positive_mitsotakis) +
                (day4[6] - average_positive_mitsotakis) * (day4[6] - average_positive_mitsotakis) +
                (day5[6] - average_positive_mitsotakis) * (day5[6] - average_positive_mitsotakis) +
                (day6[6] - average_positive_mitsotakis) * (day6[6] - average_positive_mitsotakis)) / 7.0);

        System.out.println("Mitsotakis positive: " + s);
        standard_deviation[6] = s;

        s = Math.sqrt(((day1[7] - average_negative_mitsotakis) * (day1[7] - average_negative_mitsotakis) +
                (day2[7] - average_negative_mitsotakis) * (day2[7] - average_negative_mitsotakis) +
                (day3[7] - average_negative_mitsotakis) * (day3[7] - average_negative_mitsotakis) +
                (day4[7] - average_negative_mitsotakis) * (day4[7] - average_negative_mitsotakis) +
                (day5[7] - average_negative_mitsotakis) * (day5[7] - average_negative_mitsotakis) +
                (day6[7] - average_negative_mitsotakis) * (day6[7] - average_negative_mitsotakis)) / 7.0);

        System.out.println("Mitsotakis negative: " + s);
        standard_deviation[7] = s;

        System.out.println("************************************");
    }

    /**
     * sends data to graph methods to create the diagrams
     *
     * @param n the user's choice
     */
/*
    private void createGraphs(int n) {

        //variables for positive/negative tweets/day
        int[] positive_syriza = new int[]{day1[0], day2[0], day3[0], day4[0], day5[0], day6[0], day7[0]};
        int[] negative_syriza = new int[]{day1[1], day2[1], day3[1], day4[1], day5[1], day6[1], day7[1]};
        int[] positive_nd = new int[]{day1[2], day2[2], day3[2], day4[2], day5[2], day6[2], day7[2]};
        int[] negative_nd = new int[]{day1[3], day2[3], day3[3], day4[3], day5[3], day6[3], day7[3]};
        int[] positive_tsipras = new int[]{day1[4], day2[4], day3[4], day4[4], day5[4], day6[4], day7[4]};
        int[] negative_tsipras = new int[]{day1[5], day2[5], day3[5], day4[5], day5[5], day6[5], day7[5]};
        int[] positive_mitsotakis = new int[]{day1[6], day2[6], day3[6], day4[6], day5[6], day6[6], day7[6]};
        int[] negative_mitsotakis = new int[]{day1[7], day2[7], day3[7], day4[7], day5[7], day6[7], day7[7]};

        //variables for total diagrams
        int positive_total_syriza = day1[0] + day2[0] + day3[0] + day4[0] + day5[0] + day6[0] + day7[0];
        int negative_total_syriza = day1[1] + day2[1] + day3[1] + day4[1] + day5[1] + day6[1] + day7[1];
        int positive_total_nd = day1[2] + day2[2] + day3[2] + day4[2] + day5[2] + day6[2] + day7[2];
        int negative_total_nd = day1[3] + day2[3] + day3[3] + day4[3] + day5[3] + day6[3] + day7[3];
        int positive_total_tsipras = day1[4] + day2[4] + day3[4] + day4[4] + day5[4] + day6[4] + day7[4];
        int negative_total_tsipras = day1[5] + day2[5] + day3[5] + day4[5] + day5[5] + day6[5] + day7[5];
        int positive_total_mitsotakis = day1[6] + day2[6] + day3[6] + day4[6] + day5[6] + day6[6] + day7[6];
        int negative_total_mitsotakis = day1[7] + day2[7] + day3[7] + day4[7] + day5[7] + day6[7] + day7[7];

        //variables for average datagrams
        double average_positive_syriza = (day1[0] + day2[0] + day3[0] + day4[0] + day5[0] + day6[0] + day7[0]) / 7.0;
        double average_negative_syriza = (day1[1] + day2[1] + day3[1] + day4[1] + day5[1] + day6[1] + day7[1]) / 7.0;
        double average_positive_nd = (day1[2] + day2[2] + day3[2] + day4[2] + day5[2] + day6[2] + day7[2]) / 7.0;
        double average_negative_nd = (day1[3] + day2[3] + day3[3] + day4[3] + day5[3] + day6[3] + day7[3]) / 7.0;
        double average_positive_tsipras = (day1[4] + day2[4] + day3[4] + day4[4] + day5[4] + day6[4] + day7[4]) / 7.0;
        double average_negative_tsipras = (day1[5] + day2[5] + day3[5] + day4[5] + day5[5] + day6[5] + day7[5]) / 7.0;
        double average_positive_mitsotakis = (day1[6] + day2[6] + day3[6] + day4[6] + day5[6] + day6[6] + day7[6]) / 7.0;
        double average_negative_mitsotakis = (day1[7] + day2[7] + day3[7] + day4[7] + day5[7] + day6[7] + day7[7]) / 7.0;

        switch (n) {
            case 0:
                System.exit(0);
            case 1:
                Graph chart_pos = new Graph("Positive Tweets/Day", "Days", "Number of Tweets", positive_syriza, positive_nd, positive_tsipras, positive_mitsotakis);
                chart_pos.pack();
                RefineryUtilities.positionFrameOnScreen(chart_pos, 0.7, 0.4);
                chart_pos.setVisible(true);

                Graph chart_neg = new Graph("Negative Tweets/Day", "Days", "Number of Tweets", negative_syriza, negative_nd, negative_tsipras, negative_mitsotakis);
                chart_neg.pack();
                RefineryUtilities.positionFrameOnScreen(chart_neg, 0.3, 0.4);
                chart_neg.setVisible(true);

                chooseGraph();
            case 2:
                GraphBar chart_total = new GraphBar("Total tweets per Category", "Number of Tweets", positive_total_syriza, negative_total_syriza, positive_total_nd, negative_total_nd, positive_total_tsipras, negative_total_tsipras, positive_total_mitsotakis, negative_total_mitsotakis);
                chart_total.pack();
                RefineryUtilities.centerFrameOnScreen(chart_total);
                chart_total.setVisible(true);

                chooseGraph();
            case 3:
                GraphBar chart_average = new GraphBar("Average tweets per Category", "Number of Tweets", average_positive_syriza, average_negative_syriza, average_positive_nd, average_negative_nd, average_positive_tsipras, average_negative_tsipras, average_positive_mitsotakis, average_negative_mitsotakis);
                chart_average.pack();
                RefineryUtilities.centerFrameOnScreen(chart_average);
                chart_average.setVisible(true);

                chooseGraph();
            case 4:
                GraphBar chart_s = new GraphBar("Standard deviation of tweets per Category", "Number of Tweets", standard_deviation[0], standard_deviation[1], standard_deviation[2], standard_deviation[3], standard_deviation[4], standard_deviation[5], standard_deviation[6], standard_deviation[7]);
                chart_s.pack();
                RefineryUtilities.centerFrameOnScreen(chart_s);
                chart_s.setVisible(true);

                chooseGraph();
            default:
                break;
        }
    }
*/

    /**
     * menu that show what diagram to create
     * sends option to createGraph method
     */
    private void chooseGraph() {
        int n = 0;
        do {
            Scanner reader = new Scanner(System.in);  // Reading from System.in
            System.out.println("What graphs do you want to see?(Choose 1-5)\n1.Positive and Negative tweets per day\n2.Total tweets per category\n3.Average per category\n4.Standard deviation per category\n0.Exit\n");
            System.out.print("Option: ");
            n = reader.nextInt(); // Scans the next token of the input as an int.
            if (n < 0 || n > 4) System.out.println("\nNo such option!");
        } while (n < 0 || n > 4);
        //createGraphs(n);
    }
}